var DSSV = "DSSV";
var dssv = [];
var dssvJson = localStorage.getItem(DSSV);
if (dssvJson != null) {
    var svArr = JSON.parse(dssvJson);
    dssv = svArr.map(function (item) {
        var sv = new SinhVien(item.ma, item.ten, item.matKhau, item.email, item.diemToan, item.diemLy, item.diemHoa);
        return sv;
    })
    renderDSSV(dssv);
}

function themSinhVien() {
    var sv = laythongtintuForm();
    dssv.push(sv);
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV, dssvJson);
    renderDSSV(dssv);
}

function xoaSinhVien(idSv) {
    var vitri = timKiemViTri(idSv, dssv);

    if (vitri != -1) {
        dssv.splice(vitri, 1)
        var dssvJson = JSON.stringify(dssv);
        localStorage.setItem(DSSV, dssvJson);
        renderDSSV(dssv);
    }
}

function suaSinhVien(idSv) {
    var vitri = timKiemViTri(idSv, dssv);

    if (vitri == -1) {
        return;
    }
    var sv = dssv[vitri];
    showTongTinLenForm(sv)
}

function capNhatSinhVien() {
    var sv = laythongtintuForm();
    var vitri = timKiemViTri(sv.ma, dssv);
    if (vitri != -1) {
        dssv[vitri] = sv;
        var dssvJson = JSON.stringify(dssv);
        localStorage.setItem(DSSV, dssvJson);
        renderDSSV(dssv);
    }
}