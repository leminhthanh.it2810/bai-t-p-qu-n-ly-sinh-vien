function laythongtintuForm() {
    const _maSV = document.getElementById('txtMaSV').value;
    const _tenSV = document.getElementById('txtTenSV').value;
    const _email = document.getElementById('txtEmail').value;
    const _matkhau = document.getElementById('txtPass').value;
    const _diemToan = document.getElementById('txtDiemToan').value * 1;
    const _diemLy = document.getElementById('txtDiemLy').value * 1;
    const _diemHoa = document.getElementById('txtDiemHoa').value * 1;

    var sv = new SinhVien(_maSV,_tenSV,_matkhau,_email,_diemToan,_diemLy,_diemHoa);
    return sv;
}
 
function renderDSSV(svArr) {

    var contentHTML = "";
    for (var index = 0; index < svArr.length; index++) {
        var item = svArr[index];
        var contentTr = `<tr>
                            <td>${item.ma}</td>
                            <td>${item.ten}</td>
                            <td>${item.email}</td>
                            <td>${item.tinhDTB()}</td>
                            <td>
                                <button onClick="xoaSinhVien('${item.ma}')" class="btn btn-danger">Xóa</button>
                            </td>
                            <td>
                                <button onClick="suaSinhVien('${item.ma}')" class="btn btn-warning">Sửa</button>
                            </td>
                        </tr>`;
        contentHTML += contentTr;
    }

    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function timKiemViTri(id, arr) {
    var vitri = -1;
    for (var index = 0; index < arr.length; index++) {
        var sv = arr[index];
        if (sv.ma == id) {
            vitri = index;
            break;
        }
    }
    return vitri;
}

function showTongTinLenForm(sv) {
    document.getElementById('txtMaSV').value = sv.ma;
    document.getElementById('txtTenSV').value = sv.ten;
    document.getElementById('txtEmail').value = sv.email;
    document.getElementById('txtPass').value = sv.matkhau;
    document.getElementById('txtDiemToan').value = sv.diemToan;
    document.getElementById('txtDiemLy').value = sv.diemLy;
    document.getElementById('txtDiemHoa').value = sv.diemHoa;
}